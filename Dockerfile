# Use a base image with Java installed
FROM openjdk:17

# Set the working directory inside the container
WORKDIR /app

# Copy the JAR file into the container
COPY build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /app/

# Specify the command to run your application
CMD ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]
